#!/bin/bash

#Turn on bash's job control
set -m

./mcserver  &

sleep 60;

# Start the seeder curl request
curl -k -X PUT "https://localhost:8443/api/server/configuration?deploy=true&overwriteConfigMap=true" -H "accept: application/xml"  -H "Content-Type: application/xml" --user admin:admin -d @config.xml 


# bring the primary process back into the foreground  and leave it there 
fg %1

